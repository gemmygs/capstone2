package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class CourseController {

    @Autowired
    CourseService courseService;

    // Create Course

    //@PostMapping("/courses")
    @RequestMapping(value = "/courses",method = RequestMethod.POST)
    public ResponseEntity<Object> createCourse(@RequestHeader(value = "Authorization") String stringToken,@RequestBody Course course){
        courseService.createCourse(stringToken,course);
        return new ResponseEntity<>("Course created successfully", HttpStatus.CREATED);
    }

    // Get all course
    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourse(){
        return new ResponseEntity<>(courseService.getCourse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatedCourse(@PathVariable Long courseid,@RequestHeader(value="Authorization")String stringToken, @RequestBody Course course){
        return courseService.updateCourse(courseid,stringToken, course);
    }

    // Delete a course
    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteCourse(@PathVariable Long courseid,@RequestHeader(value="Authorization") String stringToken){
        return courseService.deleteCourse(stringToken,courseid);
    }

}
